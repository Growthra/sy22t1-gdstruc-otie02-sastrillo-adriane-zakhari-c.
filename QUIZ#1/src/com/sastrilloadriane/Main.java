package com.sastrilloadriane;

public class Main {

    public static void main(String[] args) {
    int[] numbers = new int[20];

    numbers[0] = 50;
    numbers[1] = 24;
    numbers[2] = 14;
    numbers[3] = 18;
    numbers[4] = 547;
    numbers[5] = 60;
    numbers[6] = 80;
    numbers[7] = 45;
    numbers[8] = 17;
    numbers[9] = 35;
    numbers[10] = 28;
    numbers[11] = 29;
    numbers[12] = 95;
    numbers[13] = 52;
    numbers[14] = 75;
    numbers[15] = 69;
    numbers[16] = 48;
    numbers[17] = 50;
    numbers[18] = 61;
    numbers[19] = 90;

    selectionSortSmallestValue(numbers);
    printArrayElements(numbers);
}

    private static void bubbleSort(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i] < arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    private static void selectionSort(int arr[]){
        for (int i = 0 ;i < arr.length-1; i++){
            int min = i;
            for (int j = i+1; j < arr.length; j++){
                if (arr[j] > arr[min]){
                    min = j;
                }
            }
            int temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
        }
    }

    private static void selectionSortSmallestValue(int arr[])
    {
        for (int i = 0 ;i< arr.length-1; i++){
            int min = i;
            for (int j = i+1; j< arr.length; j++){
                if (arr[j] > arr[min]){
                    min = j;
                }
            }
            int temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
        }
    }

    private static void printArrayElements(int [] arr) {
        for (int j : arr) {
            System.out.println(j);
        }
    }


}

